#include <WiFi.h>
#include <TFT_eSPI.h> // Graphics and font library for ST7735 driver chip
#include <pcf8563.h>
#include <Wire.h>
#include <config.h>
#include "generated/jankywatchlogo.h"

// Settings

// WIFI_SHUTOFF_DELAY is the time (in seconds) after which the wifi will shut off when there are no clients
#define WIFI_SHUTOFF_DELAY  60


// Copied from https://github.com/Xinyuan-LilyGO/LilyGO-T-Wristband/blob/master/LilyGO-T-Wristband.ino
#define TP_PIN_PIN          33
#define I2C_SDA_PIN         21
#define I2C_SCL_PIN         22
#define IMU_INT_PIN         38
#define RTC_INT_PIN         34
#define BATT_ADC_PIN        35
#define VBUS_PIN            36
#define TP_PWR_PIN          25
#define LED_PIN             4
#define CHARGE_PIN          32
#define VREF                1100

TFT_eSPI tft = TFT_eSPI();
PCF8563_Class rtc;

uint32_t targetTime = 0;
uint8_t hour, minute, second = 0;
uint8_t rendered_minute;
uint8_t xcolon = 0;

bool pressed = false;
uint32_t pressedTime = 0;
uint32_t wifiShutoffTime;
int numberOfNetworks;
Config config;


#define SCREEN_TIME 0
int current_screen = SCREEN_TIME;

void setup() {
  Serial.begin(115200);
  Serial.println("Booting JankyWatch");

  tft.init();
  tft.setRotation(1);
  tft.setSwapBytes(true);
  tft.fillScreen(TFT_BLACK);
  tft.pushImage(0, 0, 160, 80, jankywatchlogo);
  tft.setTextColor(TFT_YELLOW, TFT_BLACK);
  // tft.setTextDatum(TL_DATUM); // unclear what this is

  pinMode(TP_PIN_PIN, INPUT);

  //! Must be set to pull-up output mode in order to wake up in deep sleep mode
  // pinMode(TP_PWR_PIN, PULLUP);
  // digitalWrite(TP_PWR_PIN, HIGH);
  // pinMode(LED_PIN, OUTPUT);


  Wire.begin(I2C_SDA_PIN, I2C_SCL_PIN);
  Wire.setClock(400000);

  // setup the RTC
  rtc.begin(Wire);
  rtc.check();

  config.load();

  delay(1000);
  tft.fillScreen(TFT_BLACK);
}

void renderTime() {
  uint8_t xpos = 15;
  uint8_t ypos = 10;

  if (rendered_minute != minute) { // Only redraw every minute to minimise flicker
    // Uncomment ONE of the next 2 lines, using the ghost image demonstrates text overlay as time is drawn over it
    tft.setTextColor(0x39C4, TFT_BLACK);  // Leave a 7 segment ghost image, comment out next line!

    //tft.setTextColor(TFT_BLACK, TFT_BLACK); // Set font colour to black to wipe image
    // Font 7 is to show a pseudo 7 segment display.
    // Font 7 only contains characters [space] 0 1 2 3 4 5 6 7 8 9 0 : .
    tft.drawString("88:88", xpos, ypos, 7); // Overwrite the text to clear it
    tft.setTextColor(0xFBE0, TFT_BLACK); // Orange

    if (hour < 10) {
      xpos += tft.drawChar('0', xpos, ypos, 7);
    }

    xpos += tft.drawNumber(hour, xpos, ypos, 7);
    xcolon = xpos;
    xpos += tft.drawChar(':', xpos, ypos, 7);
    if (minute < 10) {
      xpos += tft.drawChar('0', xpos, ypos, 7);
    }
    tft.drawNumber(minute, xpos, ypos, 7);

    rendered_minute = minute;
  }

  if(second % 2) { // Flash the colon
    tft.setTextColor(0x39C4, TFT_BLACK);
    xpos += tft.drawChar(':', xcolon, ypos, 7);
    tft.setTextColor(0xFBE0, TFT_BLACK);
  } else if(xcolon > 0){
    tft.drawChar(':', xcolon, ypos, 7);
  }
}


String getVoltage() {
  uint16_t v = analogRead(BATT_ADC_PIN);
  float battery_voltage = ((float)v / 4095.0) * 2.0 * 3.3 * (VREF / 1000.0);
  return String(battery_voltage) + "V";
}

void readRTC() {
  RTC_Date datetime = rtc.getDateTime();
  hour = datetime.hour;
  minute = datetime.minute;
  second = datetime.second;
}

void renderStatusBar() {
  int x = 1;
  int y = tft.height() - 13;
  int pt = 1;

  if(pressed) {
    x += tft.drawNumber(pressedTime, x, y, pt);
    x += tft.drawString(" | ", x, y, pt);
  }

  x += tft.drawNumber(numberOfNetworks, x, y, pt);
  x += tft.drawString(" | ", x, y, pt);
  x += tft.drawString(wifiStatusString(), x, y, pt);
  x += tft.drawString(" | ", x, y, pt);
  x += tft.drawString(getVoltage(), x, y, pt);
}

void renderScreen() {
  switch(current_screen) {
    case SCREEN_TIME:
      renderTime();
      break;
  }
  renderStatusBar();
}

void checkButton() {
  if (digitalRead(TP_PIN_PIN) == HIGH) {
    if (!pressed) {
      Serial.println("Button pressed");
      // digitalWrite(LED_PIN, HIGH);
      // delay(100);
      // digitalWrite(LED_PIN, LOW);
      pressed = true;
      pressedTime = millis();
    }
  } else {
    if(pressed) {
      Serial.println("Button unpressed");
      pressed = false;
    }
  }
}

void loop() {
  if(targetTime < millis()) {
    readRTC();
    targetTime = millis() + 1000;
  }


  wifiLoop();
  checkButton();
  renderScreen();
}


// A function to indicate that the wifi is still in use ane the automatic
// wireless shutdown should be delayed
void wifiKeepAlive() {
  wifiShutoffTime = millis() + WIFI_SHUTOFF_DELAY;
}
