#include <WiFi.h>
#include <ArduinoJson.h>
#include <ESPAsyncWebServer.h>

#define SCAN_TIMEOUT 30000
#define CONFIG_SSID "JankyWatch"
#define MIN_SCAN_INTERVAL 30000
#define WIFI_WAKUPE_INTERVAL 30000 // how frequently to scan for wifi networks, after not finding any suitable ones. One or more hours seems (3600000+) seems like a good value to use in production.

// Static web content
extern const uint8_t index_html_start[] asm("_binary_index_html_start");
extern const uint8_t index_html_size[] asm("_binary_index_html_size");

AsyncWebServer web(80);
unsigned long next_wakeup = 0;

#define JANKYWATCH_WIFI_MODE_BOOTED 0
#define JANKYWATCH_WIFI_MODE_SLEEP 1
#define JANKYWATCH_WIFI_MODE_CONFIG 2
#define JANKYWATCH_WIFI_MODE_CLIENT 3
#define JANKYWATCH_WIFI_SLEEP 4
int current_wifi_mode = 0;

void wifiLoop() {
  static unsigned long next_status;
  if(next_status < millis()) {
    Serial.printf("[%lu][wifi] status: %s", millis(), wifiStatusString().c_str());
    Serial.println();
    next_status = millis() + 1000;
  }
  switch(current_wifi_mode) {
  case JANKYWATCH_WIFI_MODE_BOOTED:
    activateConfigAP();
    break;
  case JANKYWATCH_WIFI_MODE_CONFIG:
    scanNetworks();
    break;
  case JANKYWATCH_WIFI_MODE_CLIENT:
    wifiClientModeLoop();
    break;
  case JANKYWATCH_WIFI_SLEEP:
    if(config.wifi_is_configured) {
      if(millis() < next_wakeup) {
        return;
      }
      Serial.println(F("attempting to connect"));
      WiFi.begin(config.wifi_ssid, config.wifi_psk);
      next_wakeup = millis() + MIN_SCAN_INTERVAL;
      current_wifi_mode = JANKYWATCH_WIFI_MODE_CLIENT;
    }
    break;
  }
}

void wifiSleepLoop() {
  if(millis() < next_wakeup) {
    return;
  }

  if(!config.wifi_is_configured) {
    switch(current_wifi_mode) {
    case JANKYWATCH_WIFI_MODE_BOOTED:
      activateConfigAP();
      break;
    case JANKYWATCH_WIFI_MODE_CONFIG:
      scanNetworks();
      break;
    case JANKYWATCH_WIFI_MODE_CLIENT:
      attemptConnect();
      break;
    }
    return;
  }

  WiFi.mode(WIFI_STA);
  Serial.printf("[%lu] [wifi] wake from sleep to check for SSID %s", millis(), config.wifi_ssid);
  Serial.println();
  WiFi.begin(config.wifi_ssid, config.wifi_psk);
}

void wifiClientModeLoop() {
  switch(WiFi.status()) {
    case WL_CONNECTED:
      if(!config.wifi_is_configured) {
        attemptConnect();
      }
      break;
    case WL_CONNECT_FAILED:
      Serial.printf("[%lu][wifi] connection failed, going to sleep", millis());
      Serial.println();
      break;
    case WL_CONNECTION_LOST:
      Serial.printf("[%lu][wifi] connection lost", millis());
      Serial.println();
      break;
    case WL_DISCONNECTED:
      Serial.printf("[%lu][wifi] disconnected", millis());
      Serial.println();
      break;
    default:
      Serial.printf("[%lu][wifi] unexpected connection type: %s", millis(), wifiStatusString().c_str());
      Serial.println();
      break;
  }
}

void scanNetworks() {
  static uint32_t next_scan = 0;
  if(millis() > next_scan) {
    Serial.printf("[%lu] ", millis());
    Serial.println("[wifi] Scanning for wifi networks...");
    next_scan = millis() + SCAN_TIMEOUT;
    numberOfNetworks = WiFi.scanNetworks();
    next_scan = millis() + MIN_SCAN_INTERVAL;
  }
}

String translateEncryptionType(wifi_auth_mode_t encryptionType) {
  switch (encryptionType) {
    case (WIFI_AUTH_OPEN):
      return "Open";
    case (WIFI_AUTH_WEP):
      return "WEP";
    case (WIFI_AUTH_WPA_PSK):
      return "WPA_PSK";
    case (WIFI_AUTH_WPA2_PSK):
      return "WPA2_PSK";
    case (WIFI_AUTH_WPA_WPA2_PSK):
      return "WPA_WPA2_PSK";
    case (WIFI_AUTH_WPA2_ENTERPRISE):
      return "WPA2_ENTERPRISE";
    default:
      return "??";
  }
}

String wifiStatusString() {
  switch(WiFi.status()) {
    case WL_NO_SHIELD:
      return "";
    case WL_IDLE_STATUS:
      return "-";
    case WL_NO_SSID_AVAIL:
      return "NO SSID!";
    case WL_SCAN_COMPLETED:
      return "S/C";
    case WL_CONNECTED:
      return "C";
    case WL_CONNECT_FAILED:
      return "C/F";
    case WL_CONNECTION_LOST:
      return "C/L";
    case WL_DISCONNECTED:
      return "D/C";
    default:
      return "??";
  }
}

// void jsonStatus(AsyncWebServerResponse* response, const char* key, const char* msg) {
//   StaticJsonDocument<2> err;
//   err[key] = msg;
//   serializeJson(err, *response);
// }

void activateConfigAP() {
  Serial.printf("[%lu] ", millis());
  Serial.println("[wifi] Activating the config AP");

  WiFi.mode(WIFI_MODE_AP);
  web.reset();
  WiFi.softAP(CONFIG_SSID);

  // Set up the bindings for the web server
  web.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    AsyncWebServerResponse *response = request->beginResponse_P(200, "text/html", (uint8_t*)index_html_start, (size_t)index_html_size);
    request->send(response);
  });

  web.on("/wifi/networks.json", HTTP_GET, [](AsyncWebServerRequest *request) {
    AsyncResponseStream* response = request->beginResponseStream("application/json");
    Serial.printf("[%lu] Sending network list of %d networks", millis(), numberOfNetworks);
    Serial.println();
    response->println("[");
    for (int i = 0; i < numberOfNetworks; i++) {
      StaticJsonDocument<256> network;
      network["ssid"] = WiFi.SSID(i);
      network["rssi"] = WiFi.RSSI(i);
      network["bssid"] = WiFi.BSSIDstr(i);
      network["enc"] = translateEncryptionType(WiFi.encryptionType(i));

      serializeJson(network, *response);
      serializeJson(network, Serial);

      // Print a comma and a line return after all but the first line
      if(i < numberOfNetworks-1) {
        response->print(",");
        Serial.print(",");
      }
      response->println();
      Serial.println();
    }

    response->println("]");
    request->send(response);
    Serial.println();
  });

  web.on("/wifi/connect", HTTP_POST, [](AsyncWebServerRequest *request) {
    AsyncResponseStream* response = request->beginResponseStream("application/json");

    if(!request->hasParam("ssid")) {
      StaticJsonDocument<2> err;
      err["error"] = "Missing parameter 'ssid'";
      serializeJson(err, *response);
      request->send(response);
      return;
    }
    AsyncWebParameter* ssid = request->getParam("ssid");
    config.wifi_ssid = ssid->value().c_str();

    if(request->hasParam("psk")) {
      AsyncWebParameter* psk = request->getParam("psk");
      config.wifi_psk = psk->value().c_str();
    }

    StaticJsonDocument<2> err;
    err["success"] = "See you on the other side...";
    serializeJson(err, *response);
    request->send(response);
    return;
  });

  web.begin();
  scanNetworks();
  current_wifi_mode = JANKYWATCH_WIFI_MODE_CONFIG;
}

void attemptConnect() {
  switch(WiFi.status()) {
  case WL_DISCONNECTED:
    // connection is in progress, return
    return;
  case WL_CONNECTED:
    config.wifi_is_configured = true;
    config.save();
    break;
  case WL_CONNECT_FAILED:
    delete config.wifi_ssid;
    delete config.wifi_psk;
    config.save();
    break;
  default:
    Serial.printf("[%lu][wifi] unexpected connection type: %s", millis(), wifiStatusString().c_str());
    Serial.println();
    break;
  }
}

// goToSleep powers down the wifi radio until the next scan interval
void goToSleep() {
  Serial.println("Powering down wifi radio");
  WiFi.mode(WIFI_OFF);
  next_wakeup = millis() + MIN_SCAN_INTERVAL;
}

uint8_t waitForConnectResult() {
  static unsigned long start = millis();

  if(millis() > start + config.wifi_connect_timeout) {
    Serial.println(F("Connection timed out"));
    return WL_CONNECT_FAILED;
  }

  uint8_t status = WiFi.status();
  if(status == WL_CONNECTED || status == WL_CONNECT_FAILED) {
    return status;
  }
}
