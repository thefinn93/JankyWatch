#!/usr/bin/env python3
from flask import Flask, jsonify, send_from_directory, request
import random
import time

app = Flask(__name__, static_folder='www')

@app.route('/<path:filename>')
def send_file(filename):
    return send_from_directory('../www', filename)

WIFI_NETWORKS = [{
    "s": "Legitimate network {}".format(i),
    "r": random.randint(-100, -50),
    "b": ":".join(["%02x" % random.randint(0, 256) for _ in range(6)]),
    "e": "WPA_WPA2_PSK" if random.randint(1, 10) < 9 else "Open"  # note that we can also have WPA2_PSK and probably others
    } for i in range(15)]

@app.route('/wifi/networks.json')
def networks():
    return jsonify(WIFI_NETWORKS)


@app.route("/wifi/connect", methods=["POST"])
def connect():
    body = request.json
    app.logger.debug(body)
    time.sleep(3)
    return jsonify({"success": body.get('psk') == "password"})

if __name__ == "__main__":
    app.run(debug=True)
