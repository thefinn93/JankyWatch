#!/usr/bin/env python3
import sys
import jinja2

SPA_TEMPLATES = ["index.html"]

env = jinja2.Environment(
    loader=jinja2.FileSystemLoader('www/'),
    autoescape=jinja2.select_autoescape(['html', 'xml'])
)

for t in SPA_TEMPLATES:
    with open(t, 'w') as f:
        template = env.get_template(t)
        f.write(template.render())

print("-DCOMPONENT_EMBED_TXTFILES={}".format(":".join(SPA_TEMPLATES)))

if len(sys.argv) > 1:
    print(" ".join(sys.argv[1:]))
