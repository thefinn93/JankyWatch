#include <FS.h>
#include <SPIFFS.h>
#include <ArduinoJson.h>
#include "config.h"

bool Config::load() {
  if(!SPIFFS.begin()) {
    Serial.println(F("Failed to mount SPIFFS, formatting"));
    SPIFFS.format();
    Serial.println("SPIFFS partiton initialized, re-mounting");
    if(!SPIFFS.begin()) {
      Serial.println("Formatting Didn't help");
      return false;
    }
  }

  Serial.println(F("Loading config file"));

  File file = SPIFFS.open(JANKYWATCH_CONFIG_FILENAME);
  StaticJsonDocument<JANKYWATCH_CONFIG_DOC_SIZE> doc;
  DeserializationError error = deserializeJson(doc, file);
  if(error) {
    Serial.println(F("Failed to read file, using default configuration"));
    return false;
  }

  wifi_is_configured = doc["wifi_is_configured"] | false;
  if(wifi_is_configured) {
    wifi_ssid = doc["wifi_ssid"];
    wifi_psk = doc["wifi_psk"];
  }

  wifi_rescan_interval = doc["wifi_rescan_interval"] | wifi_rescan_interval;

  file.close();
  return true;
}

bool Config::save() {
  StaticJsonDocument<JANKYWATCH_CONFIG_DOC_SIZE> doc;
  doc["wifi_is_configured"] = wifi_is_configured;
  if(wifi_is_configured) {
    doc["wifi_ssid"] = wifi_ssid;
    doc["wifi_psk"] = wifi_psk;
  }
  doc["wifi_rescan_interval"] = wifi_rescan_interval;

  Serial.println(F("Saving config file"));

  File configFile = SPIFFS.open(JANKYWATCH_CONFIG_FILENAME, "w");
  if (!configFile) {
    Serial.println(F("Failed to open config file for writing"));
    return false;
  }

  serializeJson(doc, configFile);
  return true;
}
