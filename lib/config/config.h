#ifndef JANKYWATCH_CONFIG
#define JANKYWATCH_CONFIG
#define JANKYWATCH_CONFIG_FILENAME "/jankywatch.json"
#define JANKYWATCH_CONFIG_DOC_SIZE 200


// Config consists of configurable options as well as other magic numbers
// that I didn't want to leave scattered around the code and might make
// configurable eventually.
class Config {
public:
  // User configurable
  bool wifi_is_configured;
  const char* wifi_ssid;
  const char* wifi_psk;

  // Magic numbers
  int wifi_connect_timeout = 30000;
  uint32_t wifi_rescan_interval = 30000;

  bool load();
  bool save();
};

#endif
