# JankyWatch

*Firmware for my T-Wristwatch*

This represents my efforts so far in writing some basic firmware for my T-Wristwatch.
I got as far as having it show an image on boot, then attempted to put the board back
in the housing and broke the screen. I ordered a replacement and will pick this back
up when it arrives.

To generate `src/generated/jankywatchlogo.h`, use `utils/generate-fb.py`:

```
utils/generate-fb.py images/JankyWatch.png jankywatchlogo > src/generated/jankywatchlogo.h
```
