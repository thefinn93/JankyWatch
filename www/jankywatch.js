var vm = new Vue({
  el: "#wrapper",
  data: {
    networks: [],
    selected_network: undefined,
    network_status: undefined
  },
  methods: {
    select_network: function(index) {
      console.log("network selected event", index);
      this.selected_network = index;
      this.$nextTick(function() {
        document.querySelector('.psk[data-index="' + index + '"]').focus();
      })
      // console.log("e", e);
      // console.log("this", this);
      // e.target.querySelector('.psk').focus();
    },
    connect: function(e) {
      e.preventDefault();
      console.log(e.target.dataset.index, e);
      this.networks[e.target.dataset.index].status = "connecting";
      var postdata = {ssid: e.target.dataset.ssid, psk: e.target.dataset.psk};
      console.log("POSTing to /wifi/connect", postdata);
      fetch('/wifi/connect', {
        method: 'POST',
        body: JSON.stringify(postdata),
        headers: {'Content-Type': 'application/json'}
      }).then(r => r.json()).then((r) => {
        this.networks[e.target.dataset.index].status = "See you on the other side!";
        console.log(r);
      }).catch((err) => {
        this.networks[e.target.dataset.index].status = "Error connecting";
        console.error("Error submitting SSID/password", err);
      });
    }
  }
});

fetch("/wifi/networks.json").then((r) => r.json()).then((r) => {
  vm.networks = r.map(n => {
    n.status = "";
    n.psk = "";
    return n;
  });
}).catch((e) => {
  console.error(e);
});
